\documentclass[a4paper]{article}

\usepackage[T1]{fontenc}
\usepackage[polish]{babel}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage[colorinlistoftodos]{todonotes}
\usepackage{hyperref}

\title{Przetwarzanie i Rozpoznawanie Dźwięków \\ \large Rozpoznawanie wypowiedzi z wykorzystaniem MFCC i DTW}

\author{Marcin Szymura 117230}

\date{\today}

\begin{document}
\maketitle

\section{Rozpoznawanie wypowiedzi}
\label{sec:introduction}

Skuteczne rozpoznawanie słów stanowi podstawę do realizacji takich zadań, jak sterowanie głosowe czy automatyczne tłumaczenie w czasie rzeczywistym. W oparciu o znajomość ludzkiego mechanizmu percepcji dźwięku \cite{wyklad_2}, jak również mechanizmów powstawania mowy, powstało wiele sposobów opisu głosek, sylab i wyrazów propagowanych za pośrednictwem fal dźwiękowych. Jednym z możliwych podejść jest analiza cepstralna. W niniejszej pracy zastosowano metodę \emph{Dynamic Time Warping} (DTW) do porównywania ścieżek dźwiękowych w ramach trzech zbiorów danych z próbkami wypowiedzi: imion, cyfr oraz komend.

\section{Opis programu}

W ramach projektu przygotowano program w języku Python realizujący pobranie próbek ze źródła danych, utworzenie wykresów w dziedzinach czasu i częstotliwości oraz spektrogramu i cepstrogramu w skali melowej. To stanowi podstawę do realizacji głównego celu projektu, czyli porównywania parami wszystkich ścieżek dźwiękowych metodą DTW. Dla każdego utworu wybierany jest inny utwór o najmniejszej odległości według normy $L^2$ wzdłuż najkrótszej ścieżki znalezionej przez DTW. Program ocenia własną jakość klasyfikacji poprzez zliczenie, w ilu instancjach dopasował ścieżkę wypowiedzi o tej samej treści. Opcjonalnie, rysuje również wizualizację najkrótszej ścieżki dla najlepszych dopasowań.

\begin{figure}
\centering
\includegraphics[width=1\textwidth]{Bozena_SP1M1BOZ.pdf}
\caption{\label{fig:Bozena}Przebieg czasowy, widmo, spektrogram i cepstrogram mówcy dla słowa ,,Bożena''}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=1\textwidth]{Barbara_SP1M1BAR.pdf}
\caption{\label{fig:Barbara}Przebieg czasowy, widmo, spektrogram i cepstrogram tego samego mówcy dla słowa ,,Barbara''}
\end{figure}	

\section{Obserwacje przebiegu czasowego, widma i spektrogramu}
Na Rysunkach \ref{fig:Bozena} oraz \ref{fig:Barbara} można zobaczyć wygenerowane programowo wykresy.

\subsection{Przebieg czasowy}
Na podstawie przebiegu czasowego można dostrzec poszczególne sylaby słowa. Widać również czasowe rozłożenie energii sygnału, a także czasy zanikania i narastania poszczególnych części słowa.

\subsection{Widmo sygnału}
Na podstawie widma można określić całościowy udział częstotliwości w wypowiedzi. Jest to cecha przydatna do określania płci mówiącego, jego wysokości głosu, ale może też zawierać informacje charakterystyczne dla wypowiadanego słowa.

\subsection{Spektrogram}
Spektrogram pozwala dostrzec zmieniające się w czasie części słowa. Czasami przejście pomiędzy głoskami jest skokowe, a czasami płynne. W zależności od rozmiaru okna, spektrogram może mieć lepszą lub gorszą rozdzielczość.

\subsection{Cepstrogram w skali melowej}
Analiza cepstralna pozwala odfiltrować nieistotne dla rozpoznania wypowiadanej głoski część widma odpowiadającą za pobudzenie. Dodatkowo, aby zwiększyć podobieństwo miary do percepcji człowieka, używa się nieliniowej skali częstotliwości (melowej). Wybiera się pewną niewielką liczbę współczynników cepstralnych, które są obliczane dla każdego okna i przedstawione na wykresie, z pominięciem pierwszej (opisuje składową stałą).

\section{Dynamic Time Warping}
Na przygotowanych cepstrogramach używany jest algorytm DTW. Polega on na dopasowaniu dwóch szeregów czasowych w taki sposób, aby zminimalizować sumaryczną odległość pomiędzy nimi według zadanej miary. W tym przypadku przyjęto miarę Euklidesową $L^2$. Przykładową wizualizację procedury można zobaczyć na Rysunku \ref{fig:dtw}.

\begin{figure}
\centering
\includegraphics[width=1\textwidth]{Bozena_PB1K1BOZ_dtw.pdf}
\caption{\label{fig:dtw}Wizualizacja procedury DTW dla tego samego słowa wypowiedzianego przez dwóch różnych lektorów}
\end{figure}	

\begin{table}
\centering
\begin{tabular}{l|r}
Zbiór & Jakość klasyfikacji \\\hline
Imiona & 99.50\% \\
Cyfry & 78.33\% \\
Komendy & 100.00\%
\end{tabular}
\caption{\label{tab:results}Wyniki dla różnych zbiorów danych}
\end{table}

\section{Otrzymane rezultaty}
Program uruchomiono na wszystkich trzech udostępnionych zbiorach danych. Uzyskane rezultaty pokazuje Tabela \ref{tab:results}. Bardzo wysoką jakość klasyfikacji osiągnięto dla dwóch zbiorów danych (Imiona oraz Komendy). Istotnie gorsze rezultaty otrzymano na zbiorze Cyfry. Bliższa analiza wyników pokazuje, że najczęstsze błędy to nieprawidłowa klasyfikacja par pięć-dziewięć oraz jeden-siedem. Wyrazy mają podobnie brzmiące głoski. Natomiast różnice w długości ich trwania są w zasadzie niwelowane przez algorytm DTW. Stąd też spodziewanym jest, że klasyfikacja oparta o tę metodę będzie miała trudności z poprawną decyzją.

\section{Wnioski}
Analiza cepstralna umożliwia uzyskanie bardzo dobrych rezultatów dla słów należących do małego zbioru. Występuje zależność uzyskanych wyników od rozmiaru okna, które powinno być niezbyt duże, aby dobrze oddać dynamikę sygnału. Jednocześnie, wydłuża to znacznie czas przetwarzania. Zdaniem autora warto byłoby przeprowadzić podobny eksperyment z większymi zbiorami danych, a także umożliwić dopasowanie wypowiedzi w czasie rzeczywistym.

\begin{thebibliography}{9}
\bibitem{wyklad_2}
  Łukasik E.,
  \emph{Analiza cepstralna, skale percepcyjne, obwiednie widma}. 
  \url{http://www.cs.put.poznan.pl/elukasik/PIRD17/Wyk\%b3ady/PiRD_2_2017.pdf}
  Slajdy wykładowe z przedmiotu Przetwarzanie i Rozpoznawanie Dźwięku, Wydział Informatyki, Politechnika Poznańska [dostęp \today]

\end{thebibliography}
\end{document}