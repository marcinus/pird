import enum
import os
import re
import urllib.parse
import urllib.request
import wave
from argparse import ArgumentParser

import dtw
import librosa
import librosa.display
import matplotlib.pyplot as plt
import numpy as np
import numpy.linalg
import sys


class DataSet(enum.Enum):
    NAMES = 'Imiona'
    DIGITS = 'cyfry'
    COMMANDS = 'komendy'


class Resources:
    base_data_url = 'http://www.cs.put.poznan.pl/elukasik/PIRD17/PROJEKT1/'
    instance_storage_path = '../data/'
    chart_storage_path = '../charts/'

    @staticmethod
    def get_data_url(data_set):
        return urllib.parse.urljoin(Resources.base_data_url, data_set.value + '/')

    @staticmethod
    def get_class_url(data_set, class_name):
        data_url = Resources.get_data_url(data_set)
        if data_set != DataSet.DIGITS:
            return urllib.parse.urljoin(data_url, class_name + '/')
        else:
            return data_url

    @staticmethod
    def get_class_name_regex(data_set):
        if data_set != DataSet.DIGITS:
            return r'<a href=".*?">(.*?)/</a>'
        else:
            return r'<a href=".*?">.*?_(.)_\.wav</a>'

    @staticmethod
    def get_instance_name_regex(data_set, class_name):
        if data_set != DataSet.DIGITS:
            return r'<a href=".*?">(.*?\.wav)</a>'
        else:
            return r'<a href=".*?">(.*?_' + class_name + '_\.wav)</a>'

    @staticmethod
    def get_instance_storage_folder(data_set):
        return os.path.join(Resources.instance_storage_path, data_set.value + '/')

    @staticmethod
    def get_chart_storage_folder(data_set):
        return os.path.join(Resources.chart_storage_path, data_set.value + '/')

    @staticmethod
    def create_directory_if_not_exists(dir_path):
        if not os.path.exists(dir_path):
            os.makedirs(dir_path)

    @staticmethod
    def list_files(directory, extension=''):
        return [f for f in os.listdir(directory) if re.match('.*'+extension, f,  re.IGNORECASE)]


class DTWMatcher:
    def __init__(self, tracks):
        self.tracks = tracks

    def evaluate_all(self, csv_exporter=None, display_partial=False):
        dtw_table = dict()
        best_tracks = dict()
        for track in self.tracks:
            dtw_table[track] = self.evaluate(track)
            best_tracks[track] = DTWMatcher.get_best_track_by(dtw_table[track])
            # DTWMatcher.plot_dtw(track, best_tracks[track], save=True)
            if csv_exporter:
                csv_exporter.append_result_row(track, dtw_table[track])
            if display_partial:
                message = '{0}: best match = {1} with score = {2:.2f}'.format(track.instance_name, best_tracks[track].instance_name, dtw_table[track][best_tracks[track]])
                print('  {0}'.format(message))
                if csv_exporter:
                    csv_exporter.append_log_row(message)

        return dtw_table, best_tracks

    def evaluate(self, track):
        distances = dict()
        for other_track in self.tracks:
            if other_track != track:
                distances[other_track] = DTWMatcher.dtw_tracks(track, other_track)
        return distances

    @staticmethod
    def plot_dtw(track_a, track_b, save=True, show=False):
        a = np.transpose(track_a.mfcc)
        b = np.transpose(track_b.mfcc)
        dist, cost, acc, path = dtw.dtw(a, b, dist=lambda x, y: numpy.linalg.norm(x - y, ord=2))
        plt.imshow(acc.T, origin='lower', cmap=plt.get_cmap('gray'), interpolation='nearest')
        plt.plot(path[0], path[1], 'w')
        plt.colorbar()
        plt.xlabel(track_a.instance_name)
        plt.ylabel(track_b.instance_name)
        plt.title('DTW Best Match [dist = {0:.2f}]'.format(dist))
        plt.tight_layout()
        if save:
            Track.save_plot(track_a.data_set, track_a.class_name, track_a.instance_name, 'best_matches')
        if show:
            plt.show()
        plt.clf()
        pass

    @staticmethod
    def dtw_tracks(track_a, track_b):
        a = np.transpose(track_a.mfcc)
        b = np.transpose(track_b.mfcc)
        dist, cost, acc, path = dtw.dtw(a, b, dist=lambda x, y: numpy.linalg.norm(x - y, ord=2))
        return dist

    @staticmethod
    def get_best_track_by(evaluation):
        return min(evaluation.keys(), key= lambda key: evaluation[key])


class CSVExporter:
    def __init__(self, tracks, data_set):
        csv_path = os.path.join('..', data_set.value + '.csv')
        log_path = os.path.join('..', data_set.value + '.txt')

        self.tracks = tracks
        self.fd = None
        self.log = open(log_path, 'w')

        self.init_export_file(csv_path)

    def init_export_file(self, path):
        self.fd = open(path, 'w')
        self.fd.write(',{0}\n'.format(','.join([x.instance_name for x in self.tracks])))

    def append_result_row(self, track, distances):
        self.fd.write(track.instance_name)
        for other_track in self.tracks:
            self.fd.write(',')
            if other_track != track:
                self.fd.write('{0:.2f}'.format(distances[other_track]))
        self.fd.write('\n')
        self.fd.flush()

    def append_log_row(self, row):
        self.log.write(row + "\n")
        self.log.flush()

    def close_export_file(self):
        self.fd.close()
        self.log.close()


class Track:
    def __init__(self, data_set, class_name, instance_name, window_size=1024, hop_length=400):
        self.data_set = data_set
        self.class_name = class_name
        self.instance_name = instance_name
        self.window_size = window_size
        self.hop_length = hop_length

        self.samples = None
        self.framerate = None
        self.signal = None
        self.fft = None
        self.mfcc = None

        self.load()
        self.calculate_fft()
        self.calculate_mfcc()

    def load(self):
        instance_path = os.path.join(Resources.get_instance_storage_folder(self.data_set), self.class_name, self.instance_name)
        wav_file = wave.open(instance_path, 'r')
        signal_raw = wav_file.readframes(wav_file.getnframes())
        self.samples = wav_file.getnframes()
        self.signal = np.fromstring(signal_raw, 'Int16')
        self.framerate = wav_file.getframerate()

    def calculate_fft(self):
        self.fft = np.fft.rfft(self.signal) / self.samples

    def calculate_mfcc(self):
        self.mfcc = librosa.feature.mfcc(self.signal.astype(float), hop_length=self.hop_length, sr=self.framerate, n_fft=self.window_size)[1:12]

    def plot_all(self, save=True, show=False):
        plt.subplot(2, 2, 1)
        plt.tight_layout()
        self.plot_time_domain()
        plt.subplot(2, 2, 2)
        plt.tight_layout()
        self.plot_frequency_domain()
        plt.subplot(2, 2, 3)
        plt.tight_layout()
        self.plot_spectrogram()
        plt.subplot(2, 2, 4)
        plt.tight_layout()
        self.plot_cepstrogram()
        plt.tight_layout()
        plt.suptitle('{0}: {1} [{2}, {3}]'.format(self.class_name, self.instance_name, self.window_size, self.hop_length), fontsize=16)
        plt.subplots_adjust(top=0.85, hspace=0.5)

        if save:
            Track.save_plot(self.data_set, self.class_name, self.instance_name)
        if show:
            plt.show()

        plt.clf()

    def plot_time_domain(self):
        plt.figure(1)
        plt.grid(True, linestyle=':', linewidth=0.5)
        plt.title('Time domain')
        plt.xlabel('Time [s]')
        plt.ylabel('Amplitude')
        plt.plot(np.arange(self.samples) / self.framerate, self.signal, linewidth=0.2)

    def plot_frequency_domain(self):
        plt.figure(1)
        plt.grid(True, linestyle=':', linewidth=0.5)
        plt.title('Frequency domain')
        plt.xlabel('Frequency [Hz]')
        plt.ylabel('Amplitude')
        plt.plot(np.fft.rfftfreq(self.samples) * self.framerate, abs(self.fft), linewidth=0.5)

    def plot_spectrogram(self):
        plt.figure(1)
        plt.title('Spectrogram')
        plt.xlabel('Time [s]')
        plt.ylabel('Frequency [Hz]')
        plt.specgram(self.signal, NFFT=self.window_size, Fs=self.framerate, noverlap=self.window_size-self.hop_length, mode='magnitude')
        plt.colorbar()

    def plot_cepstrogram(self):
        plt.figure(1)
        librosa.display.specshow(self.mfcc, x_axis='time', sr=self.framerate, hop_length=self.hop_length)
        plt.title('Mel-Frequency Cepstrogram')
        plt.xlabel('Time [s]')
        plt.ylabel('MFCC No.')
        plt.xticks(np.arange(0, plt.xticks()[0][-1], 0.2))
        plt.yticks(np.arange(1, len(self.mfcc), 4))
        plt.colorbar()

    @staticmethod
    def save_plot(data_set, class_name, instance_name, chart_name = ''):
        instance_basename = os.path.splitext(instance_name)[0]

        chart_folder_path = os.path.join(Resources.get_chart_storage_folder(data_set), chart_name)
        Resources.create_directory_if_not_exists(chart_folder_path)

        chart_path = os.path.join(chart_folder_path, class_name + '_' + instance_basename + '.pdf')
        plt.savefig(chart_path)

    @staticmethod
    def load_all_tracks(data_set, window_size=512, hop_size=200):
        print('Loading requested instances from dataset {0}'.format(data_set.value))

        tracks = []
        instance_names = dict()

        class_names = next(os.walk(Resources.get_instance_storage_folder(data_set)))[1]
        for class_name in class_names:
            instance_names[class_name] = Resources.list_files(os.path.join(Resources.get_instance_storage_folder(data_set), class_name), 'wav')

        for class_name in class_names:
            print('  Loading {0} instances from class {1}...'.format(len(instance_names[class_name]), class_name))
            for instance_name in instance_names[class_name]:
                track = Track(data_set, class_name, instance_name, window_size=window_size, hop_length=hop_size)
                tracks.append(track)

        print('All tracks loaded and analyzed to MFCC form.\n')

        return tracks


class DataCrawler:
    def __init__(self, data_set):
        self.data_set = data_set
        self.class_names = []
        self.instance_names = dict()

    def load(self):
        print('  Crawling class names from web page')
        self.load_class_names()
        print('  Crawling instance names from web page')
        for class_name in self.class_names:
            self.load_instance_names(class_name)
        self.create_missing_directories()
        self.load_missing_files()

    def load_class_names(self):
        index_page = DataCrawler.read_page_as_string(Resources.get_data_url(self.data_set))
        class_names_set = set()

        for match in re.finditer(Resources.get_class_name_regex(self.data_set), index_page, re.IGNORECASE):
            class_names_set.add(match.group(1))

        self.class_names = list(class_names_set)
        self.class_names.sort()

    def load_instance_names(self, class_name):
        self.instance_names[class_name] = []

        class_folder_url = Resources.get_class_url(self.data_set, class_name)
        index_page = DataCrawler.read_page_as_string(class_folder_url)

        for match in re.finditer(Resources.get_instance_name_regex(self.data_set, class_name), index_page, re.IGNORECASE):
            self.instance_names[class_name].append(match.group(1))

    def load_missing_files(self):
        for class_name in self.class_names:
            class_folder_url = Resources.get_class_url(self.data_set, class_name)
            for instance_name in self.instance_names[class_name]:
                instance_file_path = os.path.join(Resources.get_instance_storage_folder(self.data_set), class_name, instance_name)
                instance_file_url = urllib.parse.urljoin(class_folder_url, instance_name)
                if not os.path.isfile(instance_file_path):
                    print('    Downloading: ' + instance_file_url)
                    urllib.request.urlretrieve(instance_file_url, instance_file_path)

    def create_missing_directories(self):
        Resources.create_directory_if_not_exists(Resources.get_instance_storage_folder(self.data_set))
        for class_name in self.class_names:
            Resources.create_directory_if_not_exists(os.path.join(Resources.get_instance_storage_folder(self.data_set), class_name))

    @staticmethod
    def read_page_as_string(url):
        page_response = urllib.request.urlopen(url)
        return page_response.read().decode(page_response.headers.get_content_charset())


def parse_args():
    parser = ArgumentParser(description=usage())
    parser.add_argument('dataset', nargs='?', type=str, help="dataset name (one of 'Imiona', 'cyfry', 'komendy'")
    parser.add_argument('-w', '--window-size', type=int, default=512,
                        help='window size in samples to create spectrogram and cepstrogram')
    parser.add_argument('-j', '--hop-length', type=int, default=200,
                        help="hop length in samples to create spectrogram and cepstrogram")
    parser.add_argument('-o', '--offline', action='store_true', default=False,
                        help="avoid crawling instances from web, use local data")
    parser.add_argument('-p', '--plot', action='store_true', default=False,
                        help="plot 4-chart diagram and best-match DTW plot for each track")
    args = parser.parse_args()
    dataset_names = [item.value for item in DataSet]
    if args.dataset is None or args.dataset not in dataset_names:
        args.dataset = 'Imiona'
    return args


def usage():
    return "Program will cross-validate all instances in the dataset using default or given window and hop sizes. "


if __name__ == '__main__':
    args = parse_args()
    data_set = DataSet(args.dataset)

    print('----- Welcome to DTW Track Matching -----')
    print('Current dataset: {0} [window {1:d}, hop {2:d}]'.format(data_set.value, args.window_size, args.hop_length))

    if not args.offline:
        crawler = DataCrawler(data_set)
        crawler.load()
        print('Total tracks to be analyzed: {0:d}'.format(sum([len(x) for x in crawler.instance_names.values()])))

    print()

    tracks = Track.load_all_tracks(data_set, window_size=args.window_size, hop_size=args.hop_length)

    if args.plot:
        print('Plotting...')
        for track in tracks:
            sys.stdout.write('\r{0}: {1}'.format(track.class_name, track.instance_name))
            sys.stdout.flush()
            track.plot_all(save=True)
        print('\rDone!')

    print('Choosing track with minimum DTW distance for each track!')

    matcher = DTWMatcher(tracks)
    csv_exporter = CSVExporter(tracks, data_set)
    dtw_table, best_tracks = matcher.evaluate_all(csv_exporter, display_partial=True)
    csv_exporter.close_export_file()

    correctly_classified = 0
    for track in tracks:
        if best_tracks[track].class_name == track.class_name:
            correctly_classified += 1

    if len(tracks) > 0:
        print('\nCorrectly classified {0} of {1} tracks,'
              ' classification accuracy = {2:.2f}%'
              .format(correctly_classified, len(tracks), float(correctly_classified) / len(tracks) * 100))

    if args.plot:
        for track in tracks:
            DTWMatcher.plot_dtw(track, best_tracks[track], save=True)
